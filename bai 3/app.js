import {
  srcImgBlack,
  srcImgPurple,
  srcImgGold,
  zoomImg,
  const_value
} from "./export-import.js";

$(document).ready(function () {
  // biến số dung lượng sản phẩm
  var memoryFllowName = "128GB";
  // biến màu của sản phẩm
  var nameColorProduct = "SPACE BLACK";

  var subStringChange = nameColorProduct + "-" + memoryFllowName;
  // biến giá của sản phẩm sau khi đã sale
  var valueCalculate = 30000000;
  var valueTotalEach = 0;
  // biến giá của sản phẩm mặc định
  // biến số lượng sản phẩm
  var amountProduct = 1;
  var payMentList = [];
  // biến tên phụ kiện
  var nameAccessory = "";
  //biến value của phụ kiện
  //biến id của phụ kiện
  var idAcesory = "";
  var arr_def = [];
  var arr_acess_each = [];
  var arrayAccesory = { type: "accesory", data: [] };
  // biến tên của phụ kiện
  var nameAccessoryPayment = "";
  // biến count của phụ kiện đi theo sản phẩm trên array 1=miếng dán 2=tai nghe 3=sạc ứng với 3 sản phẩm
  var countAccesory1 = 0;
  var countAccesory2 = 0;
  var countAccesory3 = 0;
  // biến tổng tiền bao gồm sản phẩm + phụ kiện đã sale trên page chính
  var amountProductAccesory = 0;
  // tên phụ kiện
  var nameAccessoryChange = "";
  var srcImg;
  var valueCheckBoxTotal = 0;
  // giá trị mặc định của carousel image
  srcImg = srcImgBlack;
  // carousel image thay đổi sau khi click vào từng màu sản phẩm
  function carouselChangeColor(srcImg) {
    let carousel = $(".multiCarousel-inner");
    srcImg.forEach((img) => {
      var rowAccesory = `<div class="item">
    <div class="pad15">
      <img
        src=${img}
        class="featured-image"
        item-prop="image"
      />
    </div>
  </div>`;
      carousel.append(rowAccesory);
      // click vào ảnh ở băng chuyền thay push lên ảnh chính
      $(".item .pad15").click(function () {
        var src = $(this).find("img").attr("src");
        $(".zoom img").attr("src", src);
      });
    });
  }
  // hiện thị carousel image mặc định
  carouselChangeColor(srcImg);
  // xử lý phần phụ kiện
  $(".checkbox").click(function () {
    var parentplus = $(this).attr("id");
    idAcesory = parentplus;
    nameAccessoryChange = $(this).context.name;
    var checkBox = $(this).parent().eq(0);
    var valueCheckbox = checkBox.context.value;
    valueCheckbox = parseFloat(valueCheckbox);
    // cộng và trừ số tiền của phụ kiện
    if (checkBox.context.checked === true) {
      valueCheckBoxTotal += valueCheckbox;
      var accesoryList = {
        name: "",
        value: "",
        id: "",
        count: amountProduct,
      };
      accesoryList.name = nameAccessoryChange;
      accesoryList.value = valueCheckbox;
      accesoryList.id = idAcesory;
      arr_acess_each.push(accesoryList);
    } else {
      valueCheckBoxTotal -= valueCheckbox;

      arr_acess_each = arr_acess_each.filter((accesoryList) => {
        return accesoryList.name !== nameAccessoryChange;
      });
    }
    total();
  });

  function total() {
    // tinh sản phẩm + phụ kiện ( mỗi cái, chưa nhân)
    valueTotalEach = valueCalculate + valueCheckBoxTotal;
    amountProductAccesory = valueTotalEach * amountProduct;

    sale(amountProduct);
    $(".price").text(amountProductAccesory.toLocaleString("vi"));
  }

  // xử lý nút cộng trừ ở page chính
  function counTer() {
    // Nút trừ
    $(document).on("click", ".btn-minus", function () {
      var valueAmount = $(".amount").text();
      if (valueAmount > 1) valueAmount--;

      $(".amount").text(valueAmount);
      amountProduct = valueAmount;
      updateArrDef();
      total();
    });
    // Nút cộng
    $(document).on("click", ".btn-plus", function () {
      var valueAmount = $(".amount").text();

      valueAmount = parseFloat(valueAmount);
      valueAmount++;
      amountProduct = valueAmount;
      $(".amount").text(valueAmount);
      updateArrDef();
      total();
    });
    amountProduct = parseFloat(amountProduct);
  }

  function updateArrDef() {
    arr_acess_each.forEach((accesoryList) => {
      accesoryList.count = amountProduct;
    });
  }

  // xử lý phần mua nhiều sản phẩm sẽ đc sale
  function sale(valueAmount) {
    var saleEq0 = $(".sale p:eq(0)");
    var saleEq1 = $(".sale p:eq(1)");
    var saleEq2 = $(".sale p:eq(2)");
    if (valueAmount >= 10) {
      amountProductAccesory = amountProductAccesory * 0.8;
      saleEq2.addClass("outstanding");
      saleEq1.removeClass("outstanding");
    } else if (valueAmount >= 5) {
      amountProductAccesory = amountProductAccesory * 0.9;

      saleEq1.addClass("outstanding");
      saleEq2.removeClass("outstanding");
      saleEq0.removeClass("outstanding");
    } else if (valueAmount >= 2) {
      amountProductAccesory = amountProductAccesory * 0.97;

      saleEq0.addClass("outstanding");
      saleEq1.removeClass("outstanding");
    }
  }

  var itemsMainDiv = ".MultiCarousel";
  var itemsDiv = ".multiCarousel-inner";
  var itemWidth = "";

  $(".leftLst, .rightLst").click(function () {
    var condition = $(this).hasClass("leftLst");
    if (condition) click(0, this);
    else click(1, this);
  });

  ResCarouselSize();

  $(window).resize(function () {
    ResCarouselSize();
  });

  //this function define the size of the items
  function ResCarouselSize() {
    var incno = 0;
    var id = 0;
    var btnParentSb = "";
    var itemsSplit = "";
    var sampwidth = $(itemsMainDiv).width();
    var bodyWidth = $("body").width();
    $(itemsDiv).each(function () {
      id = id + 1;
      var itemNumbers = $(this).find(".item").length;
      btnParentSb = $(this).parent().attr("data-items");
      itemsSplit = btnParentSb.split(",");
      $(this)
        .parent()
        .attr("id", "multiCarousel" + id);

      if (bodyWidth >= 1200) {
        incno = itemsSplit[3];
      } else if (bodyWidth >= 992) {
        incno = itemsSplit[2];
      } else if (bodyWidth >= 768) {
        incno = itemsSplit[1];
      } else {
        incno = itemsSplit[0];
      }
      itemWidth = sampwidth / incno;

      $(this).css({
        transform: "translateX(0px)",
        width: itemWidth * itemNumbers,
      });
      $(this)
        .find(".item")
        .each(function () {
          $(this).outerWidth(itemWidth);
        });

      $(".leftLst").addClass("over");
      $(".rightLst").removeClass("over");
    });
  }

  //this function used to move the items
  function ResCarousel(e, el, s) {
    var leftBtn = ".leftLst";
    var rightBtn = ".rightLst";
    var translateXval = "";
    var divStyle = $(el + " " + itemsDiv).css("transform");
    var values = divStyle.match(/-?[\d\.]+/g);
    var xds = Math.abs(values[4]);
    if (e === 0) {
      translateXval = parseInt(xds) - parseInt(itemWidth * s);
      $(el + " " + rightBtn).removeClass("over");

      if (translateXval <= itemWidth / 2) {
        translateXval = 0;
        $(el + " " + leftBtn).addClass("over");
      }
    } else if (e === 1) {
      var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
      translateXval = parseInt(xds) + parseInt(itemWidth * s);
      $(el + " " + leftBtn).removeClass("over");

      if (translateXval >= itemsCondition - itemWidth / 2) {
        translateXval = itemsCondition;
        $(el + " " + rightBtn).addClass("over");
      }
    }
    $(el + " " + itemsDiv).css(
      "transform",
      "translateX(" + -translateXval + "px)"
    );
  }

  //It is used to get some elements from btn
  function click(ell, ee) {
    var Parent = "#" + $(ee).parent().attr("id");
    var slide = $(Parent).attr("data-slide");
    ResCarousel(ell, Parent, slide);
  }

  // carousel end

  // xử lý phần giá tiền theo dung lượng điện thoại
  $(".memory-product").each(function () {
    let elm = $(this);
    elm.click(function () {
      $(".memory-product").removeClass("choose-product");
      let myNameMemory = elm.text();
      memoryFllowName = myNameMemory;
      subStringChange = nameColorProduct + "-" + memoryFllowName;
      let myMemory = elm.attr("class");
      if (myMemory.includes(myNameMemory)) {
        elm.addClass("choose-product");
      }
      setRightValue();
      resetvalueamount();
    });
  });

  //  thay đổi giá tiền theo màu và dung lượng sản phẩm
  function setRightValue() {
    valueCalculate = const_value[subStringChange];
    total();
  }

  // thay đổi carousel image theo màu sản phẩm
  $(".color-iphone").click(function () {
    $(".multiCarousel-inner").empty();
    let namecolor = this.title;
    let inputImg =
      namecolor === "SPACE BLACK"
        ? srcImgBlack
        : namecolor === "DEEP PURPLE"
        ? srcImgPurple
        : namecolor === "GOLD"
        ? srcImgGold
        : "";
    srcImg = inputImg;
    carouselChangeColor(srcImg);
    $(".zoom img").attr("src", srcImg[0]);
    var myTitleColor = this.title;
    nameColorProduct = myTitleColor;
    subStringChange = nameColorProduct + "-" + memoryFllowName;
    resetvalueamount();
    setRightValue();
  });

  // trả giá trị mặc định của số lượng
  function resetvalueamount() {
    var valueAmount = 1;
    amountProduct = valueAmount;
    $(".amount").text(valueAmount);
  }

  // clik vào ô màu sản phẩm nào thì đánh dấu div đó đang được chọn
  $(".color-iphone").each(function () {
    let elm = $(this);
    elm.click(function () {
      $(".color-iphone").removeClass("border-red");
      let myCTextColor = elm.eq(0).text();
      $(".right-set").find(".subname").text(nameColorProduct);
      if (myCTextColor.includes(nameColorProduct) === true) {
        elm.addClass("border-red");
      }
    });
  });

  // click add sản phẩm vào giỏ hàng remove checked phụ kiện
  function removeChecked() {
    nameAccessory = "";
    $("#checkbox1, #checkbox2, #checkbox3").prop("checked", false);
  }

  // // giá trị mặc định của sản phẩm ban đầu khi mở page
  var myMemory = $(".memory-product").attr("value");
  $(".right-set")
    .find(".price")
    .text(parseFloat(myMemory).toLocaleString("vi"));

  counTer();
  // xử lý phần box thanh toán sản phầm

  // cột số thứ tự
  function stringStt() {
    $(".td-stt").each(function (key) {
      $(this).text(key + 1);
    });
  }

  //   đóng box thanh toán

  $(".close-icon").click(function () {
    Swal.fire({
      title: "Bạn có chắc sẽ xóa giỏ hàng",
      text: "khi xác nhận sẽ không thể hoàn tác!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        $(".payment").attr("class", "payment d-none");
        payMentList = payMentList.filter((productList) => {
          return productList.name !== "IPHONE 14 PRO MAX";
        });

        payMentList = payMentList.filter((productList) => {
          return productList.type !== "accesory";
        });

        arr_def = arr_def.filter((productList) => {
          return productList.name !== productList.name;
        });

        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
  });

  // function gop arr_def
  const combinedItems = (arr = []) => {
    const res = arr.reduce((acc, obj) => {
      let found = false;
      for (let i = 0; i < acc.length; i++) {
        if (acc[i].id === obj.id) {
          found = true;
          acc[i].count = acc[i].count + amountProduct;
        }
      }
      if (!found) {
        acc.push(obj);
      }
      return acc;
    }, []);
    return res;
  };

  // function gop element paymentList

  const combinedItem = (arr = []) => {
    const res = arr.reduce((acc, obj) => {
      let found = false;
      for (let i = 0; i < acc.length; i++) {
        if (acc[i].class === obj.class) {
          found = true;
          // trùng element thì +1 số lượng và count vào sản phầm
          acc[i].amount = acc[i].amount + amountProduct;
          if (checkbox1.checked) {
            acc[i].count_sticker = acc[i].count_sticker + amountProduct;
          }
          if (checkbox2.checked) {
            acc[i].count_airpods = acc[i].count_airpods + amountProduct;
          }
          if (checkbox3.checked) {
            acc[i].count_charger = acc[i].count_charger + amountProduct;
          }
        }
      }
      if (!found) {
        acc.push(obj);
      }
      return acc;
    }, []);
    return res;
  };

  //click add sản phẩm vào giỏ hàng
  $("#addcart-btn").click(function (e) {
    e.preventDefault();
    valueCheckBoxTotal = 0;
    var NameProduct = $(".name").text();
    var productList = {
      name: "s",
      color: "",
      memory: "",
      price: 0,
      amount: "",
      class: "",
      count_sticker: "",
      count_airpods: "",
      count_charger: "",
    };
    productList.count_sticker = countAccesory1;
    productList.count_airpods = countAccesory2;
    productList.count_charger = countAccesory3;
    productList.name = NameProduct;
    productList.color = nameColorProduct;
    productList.memory = memoryFllowName;
    productList.price = valueCalculate;
    productList.amount = amountProduct;
    productList.class = (
      NameProduct +
      nameColorProduct +
      memoryFllowName
    ).replace(/ /g, "");
    payMentList.push(productList);

    payMentList = combinedItem(payMentList);
    NameProduct = "IPHONE 14 PRO MAX";
    nameColorProduct = "SPACE BLACK";
    memoryFllowName = "128GB";
    valueCalculate = 30000000;
    // trùng element thì +1 count vào sản phầm
    if (checkbox1.checked) {
      productList.count_sticker = countAccesory1 + amountProduct;
    }
    if (checkbox2.checked) {
      productList.count_airpods = countAccesory2 + amountProduct;
    }
    if (checkbox3.checked) {
      productList.count_charger = countAccesory3 + amountProduct;
    }
    // function de check array to co chua object co type = accesory khong add
    function contains(a, obj) {
      for (var i = 0; i < a.length; i++) {
        if (a[i].type === obj) {
          return true;
        }
      }
      return false;
    }

    if (arr_acess_each.length > 0) {
      var check_contain = contains(payMentList, "accesory");
      var forarrAccesory = arr_acess_each.forEach((acess) => {
        arr_def.push(acess);
      });
      if (check_contain) {
        forarrAccesory;
        // gop array lai ( nếu có phụ kiện trùng )
        arr_def = combinedItems(arr_def);
        payMentList.forEach((payment) => {
          if (payment.type === "accesory") {
            payment.data = arr_def;
          }
        });
      } else {
        arrayAccesory.data = arr_acess_each;
        payMentList.push(arrayAccesory);
        forarrAccesory;
      }
      arr_acess_each = [];
    }

    amountProduct = 1;

    // chuyển acessory xuong cuoi cua array
    var lastRowAccesory;
    payMentList = payMentList.filter((payment) => {
      if (payment.type === "accesory") {
        lastRowAccesory = payment;
      }
      return payment.type !== "accesory";
    });

    if (lastRowAccesory) {
      payMentList.push(lastRowAccesory);
    }

    if (payMentList.length > 0) {
      $(".payment").addClass("d-block");
    }

    // khi add to cart thì xóa font-weight của sale title
    var removeOutstanding = $(".sale p");
    if (removeOutstanding.hasClass("outstanding")) {
      removeOutstanding.removeClass("outstanding");
    }

    renderPayment();

    //chọn sản phẩm bỏ vào giỏ hàng xong trả lại giá trị default

    // trả lại giá trị tên sản phẩm về default
    $(".subname").text("SPACE BLACK");

    // trả lại giá trị màu sản phẩm về default
    var colorIphone = $(".color-iphone");
    if (colorIphone.hasClass("border-red")) {
      colorIphone.removeClass("border-red");
    }
    $(".product-black").addClass("border-red");

    // trả lại giá trị bộ nhớ về default
    var memoryProduct = $(".memory-product");
    if (memoryProduct.hasClass("choose-product")) {
      memoryProduct.removeClass("choose-product");
    }
    $(".memory-128GB").addClass("choose-product");

    // giá trị mặc định của sản phẩm ban đầu khi mở page
    var myMemory = $(".memory-product").attr("value");
    $(".right-set")
      .find(".price")
      .text(parseFloat(myMemory).toLocaleString("vi"));

    // click add to cart trả về ảnh chính và carousel mặc định (màu đen)
    var src = $(".zoom").find("img").attr("src");
    $(".zoom img").attr(
      "src",
      " https://didongviet.vn/dchannel/wp-content/uploads/2022/09/1-iphone-14-pro-max-co-may-mau-didongviet.jpg"
    );
    $(".multiCarousel-inner").empty();
    srcImg = srcImgBlack;
    carouselChangeColor(srcImg);

    removeChecked();
    resetvalueamount();
  });

  //hàm sale giá sản phẩm trên giỏ hàng

  function saleCart(amount, total) {
    if (amount >= 10) {
      total = total * 0.8;
    } else if (amount >= 5) {
      total = total * 0.9;
    } else if (amount >= 2) {
      total = total * 0.97;
    }
    return total;
  }

  // render html thanh toan
  function renderPayment() {
    $("#product-details tbody").empty();
    payMentList.forEach((element) => {
      // each row san pham
      var eachRow = "";
      if (element.name) {
        var moneyProductRow = parseFloat(element.amount) * element.price;
        moneyProductRow = saleCart(element.amount, moneyProductRow);
        eachRow = `<tr class='${element.class}'>
        <td class='td-stt text-center;' ></td>
           <td colspan="2" class='name-product'>${element.name} ${
          element.color
        } ${element.memory}</td>
      <td class='box-amount'>
      <button class="btn-minus-cart" step="1"><i class="fa fa-minus i-minus" aria-hidden="true"></i></button>
      <p class="amount-cart">${element.amount}</p>
      <button class="btn-plus-cart" step="1"><i class="fa fa-plus i-plus" aria-hidden="true"></i></button>
      </td>
      <td><input disabled type='text' value='${saleCart(
        element.amount,
        element.price
      )}' step='1' class='form-control txt-don-gia' name='DonGia[]'></td>
      <td class='td-sum-amount'  style='width:100px'>
      ${moneyProductRow.toLocaleString("vi") + " VNĐ"}
      </td>
      <td class="text-center"><a href='#' class='btn-delete-sp text-danger'><i class="fa fa-trash" aria-hidden="true"></i></a><td></td>
      </tr>`;
      } else {
        var sumMonneyAccesory = 0;
        arr_def.forEach((element, index) => {
          var space = ", ";
          if (index === 0) {
            space = "";
          }
          var name_el = element.name;
          var name_count = element.count;
          nameAccessoryPayment += space + name_el + `(${name_count})`;
          sumMonneyAccesory += element.value * element.count;
          eachRow = `<tr class='accesory'>
          <td class='td-stt text-center;'></td>
          <td class='accesory-cart'>Phụ kiện</td>
          <td colspan="3" class='name-accesory'>${nameAccessoryPayment}</td>
          
          <td class='td-sum-amount'style='width:100px'>${
            sumMonneyAccesory.toLocaleString("vi") + "VNĐ"
          }</td>
          <td class="text-center"><a href='#' class='btn-delete-sp text-danger'><i class="fa fa-trash" aria-hidden="true"></i></a><td></td>
          </tr>`;
        });
        nameAccessoryPayment = "";
      }
      $("#product-details").append(eachRow);
      handlingMoney();
    });

    stringStt();
  }

  // Nút trừ
  $(document).on("click", ".btn-minus-cart", function (e) {
    var parentplus = $(this).parent().parent().attr("class");
    var findValueAmount = $(`.${parentplus}`).find("td:eq(2)").find("p");
    var myTr = $(this).parents().parents();
    var valueAmount = findValueAmount.text();
    if (valueAmount > 0) valueAmount--;

    findValueAmount.text(valueAmount);
    removeArrayCounter(parentplus, valueAmount);
    if (valueAmount === 0) {
      $(myTr).remove("tr");
      removeArray(parentplus);
    }
    renderPayment();
    sumAmount(parentplus);
    handlingMoney();
  });

  // Nút cộng
  $(document).on("click", ".btn-plus-cart", function (e) {
    var parentplus = $(this).parent().parent().attr("class");
    var findValueAmount = $(`.${parentplus}`).find("td:eq(2)").find("p");
    var valueAmount = findValueAmount.text();
    valueAmount++;
    findValueAmount.text(valueAmount);
    removeArrayCounter(parentplus, valueAmount);
    renderPayment();
    sumAmount(parentplus);
    handlingMoney();
  });

  function removeArrayCounter(parentplus, valueAmount) {
    payMentList.filter((productList) => {
      if (
        parentplus.includes(productList?.name?.toString().replace(/ /g, "")) &&
        parentplus.includes(productList?.color?.toString().replace(/ /g, "")) &&
        parentplus.includes(productList?.memory?.toString().replace(/ /g, ""))
      ) {
        productList.amount = valueAmount;
      }
    });
  }

  function sumAmount(parentplus) {
    // tính theo từng hàng
    var changeNumber = $(`.${parentplus}`).find("td:eq(3)").find("input").val();
    var amountSp = $(`.${parentplus}`).find("td:eq(2)").find("p").text();
    var productPayment = changeNumber * amountSp;
    $(`.${parentplus}`)
      .find("td:eq(4)")
      .text(productPayment.toLocaleString("vi") + " VNĐ");
  }

  // tính tổng tiền tất cả sản phẩm ( tất cả )
  function handlingMoney() {
    var sumAmountsp = 0;
    $("#product-details tbody tr").each(function () {
      var sumAmountEach = $("#product-details tbody tr")
        .find(".td-sum-amount")
        .text()
        ? $(this).find(".td-sum-amount").text()
        : 0;
      sumAmountEach = sumAmountEach.replace(/[^\w\s]/gi, "");
      sumAmountEach = sumAmountEach ? parseFloat(sumAmountEach) : 0;
      sumAmountsp += parseFloat(sumAmountEach);
    });
    $(".all-amount-sp").text(sumAmountsp.toLocaleString("vi") + " VNĐ");
    var tax = sumAmountsp / 10;
    $(".tax-amount-sp").text(tax.toLocaleString("vi") + " VNĐ");
    var results = sumAmountsp - tax;
    $(".result-amount-sp").text(results.toLocaleString("vi") + " VNĐ");
  }

  // nhấn buy nơw hiển thị phương thức thanh toán
  $(document).on("click", "#buy-btn", function (e) {
    e.preventDefault(e);
    Swal.fire({
      title: "bạn chọn phương thức thanh toán ?",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Thẻ ATM",
      denyButtonText: `Thanh toán khi nhận hàng`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire("Saved!", "", "success");
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
      handlingMoney();
    });
  });

  // Xóa Tr khi click
  $(document).on("click", ".btn-delete-sp", function (e) {
    e.preventDefault(e);
    var parentplus = $(this).parent().parent().attr("class");

    var countAccesoryListSticker = 0;
    var countAccesoryListAir = 0;
    var countAccesoryListCharger = 0;
    Swal.fire({
      title: "Bạn có chắc sẽ xóa sản phẩm?",
      text: "khi xác nhận sẽ không thể hoàn tác!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        var countDan = 0;
        var countAir = 0;
        var countSac = 0;
        // nếu xóa sản phẩm nào thì xóa row đó + xóa luôn trên array và tính toán lại số lượng phụ kiện tổng + tổng tiền
        payMentList.forEach((productList) => {
          if (
            productList.name &&
            parentplus.includes(productList.name.replace(/ /g, "")) &&
            parentplus.includes(productList.color.replace(/ /g, "")) &&
            parentplus.includes(productList.memory.replace(/ /g, ""))
          ) {
            removeArray(parentplus);
            countDan = productList.count_sticker;
            countAir = productList.count_airpods;
            countSac = productList.count_charger;
            // tính toán count tổng
            arr_def.forEach((accesoryList) => {
              var nameAccesoryList = accesoryList.name;
              var countAccesoryList = accesoryList.count;
              if (nameAccesoryList === "miếng dán cường lực") {
                accesoryList.count = countAccesoryList - countDan;
                countAccesoryListSticker = accesoryList.count;
              }
              if (nameAccesoryList === "Airpods Pro") {
                accesoryList.count = countAccesoryList - countAir;
                countAccesoryListAir = accesoryList.count;
              }
              if (nameAccesoryList === "Sạc dự phòng") {
                accesoryList.count = countAccesoryList - countSac;
                countAccesoryListCharger = accesoryList.count;
              }
            });
            // khi count về 0 tự động xóa element tương ứng
            if (countAccesoryListSticker === 0) {
              arr_def = arr_def.filter((productList) => {
                return productList.name !== "miếng dán cường lực";
              });
            }
            if (countAccesoryListAir === 0) {
              arr_def = arr_def.filter((productList) => {
                return productList.name !== "Airpods Pro";
              });
            }
            if (countAccesoryListCharger === 0) {
              arr_def = arr_def.filter((productList) => {
                return productList.name !== "Sạc dự phòng";
              });
            }
          }
          if (parentplus === "accesory") {
            arr_def = arr_def.filter((productList) => {
              payMentList.forEach((productList) => {
                productList.count_airpods = 0;
                productList.count_charger = 0;
                productList.count_sticker = 0;
              });
              return productList.name !== productList.name;
            });
          }
        });
        var myTr = $(this).parent().parent();
        myTr.remove();
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
      handlingMoney();
      renderPayment();
    });
  });

  // xóa element array khi trùng
  function removeArray(parentplus) {
    payMentList = payMentList.filter((productList) => {
      return !(
        parentplus.includes(productList?.name?.toString().replace(/ /g, "")) &&
        parentplus.includes(productList?.color?.toString().replace(/ /g, "")) &&
        parentplus.includes(productList?.memory?.toString().replace(/ /g, ""))
      );
    });
  }

  zoomImg();
});
