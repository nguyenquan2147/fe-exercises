// biến ảnh carousel image của Iphong màu đen
export var srcImgBlack = [
    "https://cdn.mobilesyrup.com/wp-content/uploads/2022/09/iphone-14-pro-header-1-2048x1151.jpg",
    "https://cdn.mobilesyrup.com/wp-content/uploads/2022/09/iphone-14-pro-max-rear-2048x1536.jpg",
    "https://mobilesyrup.com/wp-content/uploads/2022/09/iphone-14-pro-side-scaled.jpg",
    "https://mobilesyrup.com/wp-content/uploads/2022/09/iphone-14-pro-dynamic-island-scaled.jpg",
    "https://cdn.mobilesyrup.com/wp-content/uploads/2022/09/iphone-14-dynamic-island-2048x1536.jpg",
    "https://i.pcmag.com/imagery/reviews/03POP0TjDjuXonJXI16Omn2-14.fit_lim.size_960x.jpg",
    "https://i.pcmag.com/imagery/reviews/03POP0TjDjuXonJXI16Omn2-10.fit_lim.size_960x.jpg",
    "https://i.pcmag.com/imagery/reviews/03POP0TjDjuXonJXI16Omn2-2.fit_lim.size_960x.jpg",
  ];



  // biến ảnh carousel image của Iphong màu tím
  export  var srcImgPurple = [
    "https://images.fpt.shop/unsafe/filters:quality(90)/fptshop.com.vn/uploads/images/tin-tuc/149710/Originals/iphone-14-pro-max-15.jpg",
    "https://images.fpt.shop/unsafe/filters:quality(90)/fptshop.com.vn/uploads/images/tin-tuc/149710/Originals/iphone-14-pro-max-2.jpg",
    "https://images.fpt.shop/unsafe/filters:quality(90)/fptshop.com.vn/uploads/images/tin-tuc/149710/Originals/iphone-14-pro-max-13.jpg",
    "https://images.fpt.shop/unsafe/filters:quality(90)/fptshop.com.vn/uploads/images/tin-tuc/149710/Originals/iphone-14-pro-max-8.jpg",
    "https://images.fpt.shop/unsafe/filters:quality(90)/fptshop.com.vn/uploads/images/tin-tuc/149710/Originals/iphone-14-pro-max-11.jpg",
    "https://image.thanhnien.vn/w2048/Uploaded/2023/xdrkxrvekx/2023_01_04/ip11-6337.png",
    "https://kenh14cdn.com/203336854389633024/2022/9/8/photo-4-1662617927019804827765.jpg",
    "https://kenh14cdn.com/203336854389633024/2022/9/8/photo-8-1662617927488496692884.jpg",
  ];
 

  // biến ảnh carousel image của Iphong màu vàng
  export var srcImgGold = [
    "https://kenh14cdn.com/203336854389633024/2022/1/10/26994692320472758220936722448163515653592321n-1641788272665975683210.png",
    "https://kenh14cdn.com/203336854389633024/2022/1/10/2706863142501349372529013749732561080513738n-16417882829231190416311.png",
    "https://mavangvn.vn/wp-content/uploads/2022/09/iPhone-14-Pro-Max-8-768x432.jpg",
    "https://mavangvn.vn/wp-content/uploads/2022/09/iPhone-14-Pro-Max-1-768x432.jpg",
    "https://mavangvn.vn/wp-content/uploads/2022/09/iPhone-14-Pro-Max-10-768x432.jpg",
    "https://sohanews.sohacdn.com/thumb_w/1000/160588918557773824/2021/9/28/photo1632798627119-16327986271911693706404.jpg",
    "https://sohanews.sohacdn.com/160588918557773824/2021/9/28/photo-1-1632798573837770892743.jpg",
    "https://lh6.googleusercontent.com/POigEKwS0EX6cj6Jra2YXoE9I2_tNIWV598qP5kg0Vq346FzL1PAIV3w4DcSThlt-s7GOGaAFNVvnoc9zFPzVL4G0FwzvwXobBHjGMNo-aQOtE1NmH6XmREwABOkmCUr3YUbsEoj=s0",
  ];
  
 // xử lý phần zoom ảnh
 export function zoomImg() {
    let zoom = document.querySelector(".d-block .zoom");
    let imgZoom = document.querySelector(".d-block .imgZoom");

    zoom.addEventListener("mousemove", (event) => {
      imgZoom.style.opacity = 1;
      let positionPx = event.x - zoom.getBoundingClientRect().left;
      let positionX = (positionPx / zoom.offsetWidth) * 100;

      let positionPy = event.y - zoom.getBoundingClientRect().top;
      let positionY = (positionPy / zoom.offsetHeight) * 100;

      imgZoom.style.setProperty("--zoom-x", positionX + "%");
      imgZoom.style.setProperty("--zoom-y", positionY + "%");

      let transformX = -(positionX - 50) / 3.5;
      let transformY = -(positionY - 50) / 3.5;
      imgZoom.style.transform = `scale(1.5) translateX(${transformX}%) translateY(${transformY}%)`;
    });
    zoom.addEventListener("mouseout", () => {
      imgZoom.style.opacity = 0;
    });
  }

  // giá trị mặc định từng sản phẩm theo dung lượng và màu 
  export var const_value = {
    "SPACE BLACK-128GB": 30000000,
    "SPACE BLACK-256GB": 34000000,
    "SPACE BLACK-512GB": 40000000,
    "SPACE BLACK-1TB": 45000000,
    "DEEP PURPLE-128GB": 33000000,
    "DEEP PURPLE-256GB": 37000000,
    "DEEP PURPLE-512GB": 43000000,
    "DEEP PURPLE-1TB": 48000000,
    "GOLD-128GB": 40000000,
    "GOLD-256GB": 44000000,
    "GOLD-512GB": 50000000,
    "GOLD-1TB": 55000000,
  };
  

 