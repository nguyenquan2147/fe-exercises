$(document).ready(function () {
  // cột số thứ tự
  function stringStt() {
    $(".td-stt").each(function (key, value) {
      $(this).text(key + 1);
    });
  }
  
  // on click thêm sản phẩm
  $(".btn-add-sp").click(function (e) {
    e.preventDefault();
    var myTr = $(this).parent().parent();
    var id_sp = myTr.context.id;
    var tenSp = myTr.context.name;
    var gia_Sp = myTr.context.id;
    var giaSp = myTr.context.value;
    giaSp = giaSp === "" ? 0 : parseFloat(giaSp);

    var classNameProduct = id_sp;
    var classAmountProduct = gia_Sp;
    var newRow = `<tr class='${classNameProduct}'>
          <td class='td-stt text-center;' ></td>
          <td style='font-size: 14px;
                     line-height: 32px;
                     font-weight: 600;'></td>
            <td style=' display: flex; justify-content: center;'>
              <button style='height: 30px'  class="btn-minus" step="1"><i class="fa fa-minus i-minus" aria-hidden="true"></i></button>
              <p style='font-size: 21px;
              height: 30px;
              border: 1px solid #000;
              text-align: center;
              width: 100px;
              line-height: 30px' class="amount">1</p>
              <button style='height: 30px' class="btn-plus" step="1"><i class="fa fa-plus i-plus" aria-hidden="true"></i></button>
            </td>
            <td><input type='number' value='' step='1' class='form-control ${classAmountProduct} txt-don-gia' name='DonGia[]'></td>
            <td class='td-sum-amount'>0 VNĐ</td>
            <td class="text-center"><a href='#' class='btn-delete-sp text-danger'><i class="fa fa-trash" aria-hidden="true"></i></a><td></td>
            </tr>`;
    $("#product-details").append(newRow);
    $(`.${classNameProduct}`).find("td:eq(1)").text(tenSp);
    $(`.${classNameProduct}`).find("td:eq(3)").find("input").val(giaSp);
    // Ngay khi ấn thêm sản phẩm, thì mặc định là tính tiền 1 sản phẩm
    sumAmount(1, classNameProduct);
    var obj = {};
    $("tr").each(function () {
      var x = $(this).attr("class");
      if (obj[x]) {
        $(this).remove("tr");
        var classNameProduct = $(this).last().attr("class");
        var valueAmount = $(`.${classNameProduct}`)
          .find("td:eq(2)")
          .find("p")
          .text();
        valueAmount++;
        $(`.${classNameProduct}`).find("td:eq(2)").find("p").text(valueAmount);
        sumAmount(valueAmount, classNameProduct);
      } else {
        obj[x] = true;
      }
    });
    stringStt();
  });

  // Xóa Tr khi click
  $(document).on("click", ".btn-delete-sp", function (e) {
    e.preventDefault();
    Swal.fire({
      title: "Bạn có chắc sẽ xóa sản phẩm?",
      text: "khi xác nhận sẽ không thể hoàn tác!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        var myTr = $(this).parent().parent();
        myTr.remove();
        sumAmount();
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
    sumAmount();
  });
  // tinh tong tien
  function sumAmount(valueAmount, classNameProduct) {
    var sumAmountsp = 0;

    // co mot cach nua la khong dung vong for( lam sau )
    $("#product-details tbody tr").each(function () {
      // tinh theo tung hang
      var myTr = $(this).parent().parent();
      var donGia = $(`.${classNameProduct}`)
        .find("td:eq(3)")
        .find("input")
        .val();
      // donGia = (donGia === '' ? 0 : parseFloat(donGia));
      donGia = donGia ? parseFloat(donGia) : 0;
      var thanhTien = valueAmount * donGia;
      var thanhTienLocaleString = thanhTien
        ? thanhTien.toLocaleString("vi") + " VNĐ"
        : 0 + " VNĐ";
      $(`.${classNameProduct}`).find("td:eq(4)").text(thanhTienLocaleString);
      $(`.${classNameProduct}`).find("td:eq(4)").val(thanhTien);

      // tinh tong tien san pham ( tat ca)
      var sumAmountEach = $(this).find(".td-sum-amount").val()
        ? $(this).find(".td-sum-amount").val()
        : 0;
      // sumAmountsp += parseFloat(sumAmountEach);
      sumAmountsp = sumAmountsp + parseFloat(sumAmountEach);
    });
    $(".all-amount-sp").text(sumAmountsp.toLocaleString("vi") + " VNĐ");
    var tax = sumAmountsp / 10;
    $(".tax-amount-sp").text(tax.toLocaleString("vi") + " VNĐ");
    var results = sumAmountsp - tax;
    $(".result-amount-sp").text(results.toLocaleString("vi") + " VNĐ");
  }

  // XỬ LÝ NÚT NHẬP SỐ LƯỢNG
  // nút trừ
  $(document).on("click", ".btn-minus", function () {
    var classNameProduct = $(this).parents("tr").last().attr("class");
    var myTr = $(this).parents().parents();
    var valueAmount = $(`.${classNameProduct}`)
      .find("td:eq(2)")
      .find("p")
      .text();

    if (valueAmount > 0) valueAmount--;
    $(`.${classNameProduct}`).find("td:eq(2)").find("p").text(valueAmount);
    if (valueAmount === 0) $(myTr).remove("tr");
    sumAmount(valueAmount, classNameProduct);
  });

  // nút cộng
  $(document).on("click", ".btn-plus", function () {
    var classNameProduct = $(this).parents("tr").last().attr("class");
    // console.log(classNameProduct);
    var valueAmount = $(`.${classNameProduct}`)
      .find("td:eq(2)")
      .find("p")
      .text();
    valueAmount++;
    $(`.${classNameProduct}`).find("td:eq(2)").find("p").text(valueAmount);
    //valueAmount là số lượng sản phẩm, classNameProduct là id của từng sản phẩm
    sumAmount(valueAmount, classNameProduct);
    stringStt();
  });
});

