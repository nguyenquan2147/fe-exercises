// chuyển đổi trang thái icon .eyes password 
$(document).ready(function(){
    $('.eyes').click(function(){
        $(this).toggleClass('open');
        $(this).children('i').toggleClass('fa-eye-slash fa-eye eye');
        if($(this).hasClass('open')) {
            $(this).prev().attr('type', 'text');
        }else {
            $(this).prev().attr('type', 'password');
        }
    });
});

        function Validator(options) {

            function validate(inputElement,rule) {
                var errorElement = inputElement.parentElement.querySelector('.form-message ');
                var errorMessage =rule.test(inputElement.value);
                if (errorMessage) {
                    errorElement.innerText = errorMessage;
                    inputElement.parentElement.classList.add("invalid");
                } else {
                    errorElement.innerText = '' ;
                    inputElement.parentElement.classList.remove("invalid");
                }

            }
            var formElement = document.querySelector(options.form);
             if (formElement) {

                formElement.onsubmit = function(e) {
                    e.preventDefault();
                    // lặp qua các rules và validate luôn
                    options.rules.forEach(function(rule) {
                         var inputElement = formElement.querySelector(rule.selector);
                         validate(inputElement, rule); 
                    });
                    
                };

                // blur 
               options.rules.forEach(function(rule){
                var inputElement = formElement.querySelector(rule.selector);
                if (inputElement) {
                    inputElement.onblur = function() {
                       validate(inputElement, rule); 
                    }
                }
               });
              
             }
        };
        

        // định nghĩa cho Validator
        Validator.isRequired = function(selector) {
            return {
                selector: selector,
                test: function(value) {
                    return value ? undefined : 'vui lòng nhập dữ liệu' 
                }
            }
        };
        
        Validator.isEmail = function(selector) {
            return {
                selector: selector,
                test: function(value) {
                    var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                    return regex.test(value) ? undefined : 'đây không phải Email';
                }   
            }
        };
        
        Validator.isphone = function(selector) {
            return {
                selector: selector,
                test: function(value) {
                    var checkNumber = /(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})\b/;
                    return checkNumber.test(value) ? undefined : 'vui lòng nhập lại';
                }
            }
        };

        Validator.isPassword = function(selector) {
            return {
                selector: selector,
                test: function(value) { 
                    return value ? undefined : 'Mật khâu không chính xác' 
                }
            }
        };

        Validator ({
            form: '#form-1',
            rules: [
                Validator.isRequired('#full-name'),
                Validator.isEmail('#email'),
                Validator.isphone('#phone-number'),
                Validator.isPassword('#password')
            ]
        });    
    